# Start here

The future of democratic governance is being made in social movements, blockchains, cooperatives, and governments around the world.

**What tools do they have to work with?**

# Democratic Mediums is a directory of patterns for decision, deliberation, and debate.
