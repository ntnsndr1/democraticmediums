# Democratic Mediums

*Patterns for decision, deliberation, and noise*

A project of the [Media Enterprise Design Lab](http://cmci.colorado.edu/medlab/) at the University of Colorado Boulder.

Live demo: [medlabboulder.gitlab.io/democraticmediums](https://medlabboulder.gitlab.io/democraticmediums/)

## To do

* Load initial Mediums pages
* Information here and on the website on contributing
